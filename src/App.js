import React from "react";
import { Route, Router, Routes } from "react-router-dom";
import "./App.css";

import LoginPage from "./components/Login";
import MovieDetail from "./components/MovieDetail";
import Navbar from "./components/Navbar";
import RegisterPage from "./components/Register";
import Home from "./pages/Home";
import Game from "./pages/Game";
import AddGame from "./components/AddGame";
import MenuGame from "./components/MenuGame";
import UpdateGame from "./components/UpdateGame";
import AddGenre from "./components/AddGenre";
import Add from "./components/Add";
import AddMovie from "./components/AddMovie";
import Category from "./pages/Category";
import UpdateMovie from "./components/UpdateMovie";
import MyList from "./components/MyList";
import TvShow from "./pages/TvShow";
import AddChannel from "./components/AddChannel";
import UpdateChannel from "./components/UpdateChannel";

function App() {
  return (
    <div>
      <div className="mb-3">
        <Navbar />
      </div>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/register" element={<RegisterPage />} />
        <Route path="/Login" element={<LoginPage />} />
        <Route path="/movie/:id" element={<MovieDetail />} />
        <Route path="/update/:id" element={<UpdateMovie />} />
        <Route path="/addMovie" element={<AddMovie />} />
        <Route path="/game" element={<Game />} />
        <Route path="/addGame" element={<AddGame />} />
        <Route path="/updateGame/:id" element={<UpdateGame />} />
        <Route path="/addGenre" element={<AddGenre />} />
        <Route path="/add" element={<Add />} />
        <Route path="/genre/id/:id" element={<Category />} />
        <Route path="/mylist" element={<MyList />} />
        <Route path="/tvshow" element={<TvShow />} />
        <Route path="/addChannel" element={<AddChannel />} />
        <Route path="/updateChannel/:id" element={<UpdateChannel />} />
      </Routes>
    </div>
  );
}

export default App;
