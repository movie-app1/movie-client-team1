import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/api";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { storage } from "../Firebase";
import UpdateMovie from "./UpdateMovie";

function UpdateChannel() {
  const navigate = useNavigate();
  const { id } = useParams();
  // console.log(id);

  const [tv, setTv] = useState("");
  const [image, setImage] = useState("");

  const UpdateChannel = async (downloadURL) => {
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        tv: tv,
        image: downloadURL,
      };

      Swal.fire({
        title: "Do you want to save changes?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `cancel,`,
      }).then((result) => {
        if (result.isConfirmed) {
          // console.log(data);
          axios.put(`tv/${id}`, data);

          navigate("/tvshow");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Menu changed successfully!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Changes are not saved!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  const save = () => {
    // untuk storage
    const storageRef = ref(storage, `Channels/${image.name}`);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadURL) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          UpdateChannel(downloadURL);
          console.log(downloadURL);
        });
      });
  };

  const getDataById = async () => {
    const { data } = await axios.get(`/tv/id/${id}`);

    setTv(data.tv);
    setImage(data.image);
    // console.log(data);
  };

  useEffect(() => {
    getDataById();
  }, [id]);

  return (
    <div>
      <div>
        <section className="get-in-touch container">
          <h1 className="title fs-5 text-light">Update Channel</h1>
          <form className="contact-form row">
            <div className="form-field col-lg-6">
              <input
                id="name"
                className="input-text js-input"
                defaultValue={tv}
                onChange={(e) => setTv(e.target.value)}
                type="text"
              />
              <label className="label" htmlFor="name">
                Channel
              </label>
            </div>
            <div className="form-field col-lg-6 ">
              <input
                type="file"
                className="form-control"
                id="inputGroupFile02"
                accept="image/png, image/jpeg, image/jpg"
                onChange={(e) => setImage(e.target.files[0])}
              />
              <label className="label" htmlFor="text">
                Image
              </label>
            </div>
            <div className="form-field col-lg-12">
              <button
                onClick={save}
                className="btn btn-secondary"
                type="button"
              >
                Save
              </button>
              {/* <input
                  className="submit-btn"
                  type="submit"
                  onClick={UpdateGame}
                /> */}
            </div>
          </form>
        </section>
      </div>
    </div>
  );
}

export default UpdateChannel;
