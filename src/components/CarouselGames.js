import React from "react";
import "../css/CarouselGames.css";

function CarouselGames() {
  return (
    <div>
      <h1>haii</h1>
      <div
        id="carouselExampleControls"
        class="carousel slide container"
        data-bs-ride="carousel"
      >
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img
              src="https://cms-games.mncplus.com/storage/uploaded/img/banner//2022-09-09%2014:51:51-new_banner.png"
              class="d-block w-100"
              alt="..."
            />
          </div>
          <div class="carousel-item">
            <img
              src="https://cms-games.mncplus.com/storage/uploaded/img/banner//2023-01-17%2012:06:13-banner_(15)1.png"
              class="d-block w-100"
              alt="..."
            />
          </div>
          <div class="carousel-item">
            <img
              src="https://cms-games.mncplus.com/storage/uploaded/img/banner//2023-03-13%2017:30:40-5_6086733609287288626.png"
              class="d-block w-100"
              alt="..."
            />
          </div>
        </div>
        <button
          class="carousel-control-prev"
          type="button"
          data-bs-target="#carouselExampleControls"
          data-bs-slide="prev"
        >
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button
          class="carousel-control-next"
          type="button"
          data-bs-target="#carouselExampleControls"
          data-bs-slide="next"
        >
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
    </div>
  );
}

export default CarouselGames;
