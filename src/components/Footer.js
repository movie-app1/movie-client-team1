import React from "react";
import "../css/Footer.css";

function Footer() {
  return (
    <div className="mt-5 text-decoration-none">
      <section>
        <footer className="bg-dark text-white">
          <div className="container p-4">
            <div className="row">
              <div className="poo">
                <div className="container text-light mt-5">
                  <div className="row">
                    <div className="col-3">
                      <div className="table text-light">
                        <img
                          src="https://www.visionplus.id/statics/app_logo.png"
                          alt=""
                          width={"100px"}
                          className="row"
                        />
                        <div className="row">
                          <div className="col-6">
                            <b className="row mt-4">About Us</b>
                            <b className="row">Media Center</b>
                            <b className="row">Contact Us</b>
                          </div>
                          <div className="col-6">
                            <b className="row mt-4">FAQ</b>
                            <b className="row">Terms & Condition</b>
                            <b className="row">Privacy Policy</b>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-3">
                      <p className="text-secondary">Find us on:</p>
                      <div className="d-flex justify-content-start mb-4">
                        <i className="fa-brands fa-twitter fa-xl mx-2"></i>{" "}
                        <i className="fa-brands fa-instagram fa-xl mx-2"></i>{" "}
                        <i className="fa-brands fa-facebook fa-xl mx-2"></i>
                      </div>
                      <img
                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/2560px-Google_Play_Store_badge_EN.svg.png"
                        className="w-50 px-1"
                        alt=""
                      />
                      <img
                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/2560px-Download_on_the_App_Store_Badge.svg.png"
                        className="w-50 px-1"
                        alt=""
                      />
                      <img
                        src="https://hellopaisa.co.za/hellopaisa-2021/wp-content/uploads/2021/06/huawei-Badge-Black.png"
                        className="w-50 px-1 mt-2"
                        alt=""
                      />
                    </div>
                  </div>
                </div>
              </div>

              <p></p>
            </div>
          </div>
        </footer>
        {/* <!-- Footer --> */}
      </section>
    </div>
  );
}

export default Footer;
