import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { instance as axios } from "../util/api";
import { storage } from "../Firebase";

function AddMovie() {
  const navigate = useNavigate();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [cover, setCover] = useState(null);
  const [trailer, setTrailer] = useState("");
  const [author, setAuthor] = useState("");
  const [rating, setRating] = useState("");
  const [realease, setRealease] = useState("");
  const [genre, setGenre] = useState([]);
  const [selectGenre, setSelectGenre] = useState("");

  const getGenre = async () => {
    try {
      const { data, status } = await axios.get(`genre`);
      if (status === 200) {
        setGenre(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getGenre();
  }, []);

  const postMovie = async (downloadURL) => {
    try {
      const formData = {
        title: title,
        description: description,
        cover: downloadURL,
        author: author,
        genre: { id: selectGenre },
        trailer: trailer,
        rating: rating,
        realease: realease,
      };
      await axios.post(`movie/add`, formData);
    } catch (err) {
      console.log(err);
    }
    navigate("/");
  };

  const submit = (event) => {
    // untuk storage
    const storageRef = ref(storage, `images/${cover.name}`);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, cover)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadUrl) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          postMovie(downloadUrl);
          console.log(downloadUrl);
        });
      });
  };

  const save = (e) => {
    e.preventDefault();
    submit();
    Swal.fire({
      icon: "success",
      title: "Succes Add Book",
      showConfirmButton: false,
      timer: 800,
    });
  };
  return (
    <div>
      <section className="get-in-touch container">
        <h1 className="title fs-5">Tambah</h1>
        <form className="contact-form row">
          <div className="form-field col-lg-6">
            <input
              id="name"
              className="input-text js-input"
              defaultValue={title}
              onChange={(e) => setTitle(e.target.value)}
              type="text"
            />
            <label className="label" htmlFor="name">
              Title
            </label>
          </div>
          <div className="form-field col-lg-6 ">
            <input
              id="text"
              className="input-text js-input"
              defaultValue={author}
              onChange={(e) => setAuthor(e.target.value)}
              type="text"
            />
            <label className="label" htmlFor="text">
              Sutradara
            </label>
          </div>
          <div className="form-field col-lg-6">
            <input
              id="company"
              className="input-text js-input"
              defaultValue={trailer}
              onChange={(e) => setTrailer(e.target.value)}
              type="text"
            />
            <label className="label" htmlFor="company">
              Trailer
            </label>
          </div>
          <div className="form-field col-lg-6">
            <input
              type="file"
              className="form-control"
              id="inputGroupFile02"
              accept="image/png, image/jpeg, image/jpg"
              onChange={(e) => setCover(e.target.files[0])}
            />
            <label className="label" htmlFor="phone">
              Cover
            </label>
            {/* <input
              id="phone"
              type="file"
              className="js-input"
            />*/}
          </div>
          <div className="form-field col-lg-12">
            <select
              className="form-select  form-control-lg "
              aria-label="Default select example"
              name="genre"
              defaultValue={selectGenre}
              onChange={(e) => {
                setSelectGenre(e.target.value.toString());
              }}
              required
            >
              <option selected disabled value={""}>
                Select Genre
              </option>
              {genre.map((gen, index) => (
                <option value={gen.id} key={index}>
                  {gen.genre}
                </option>
              ))}
            </select>

            <label className="label" htmlFor="phone">
              Genre
            </label>
          </div>
          <div className="form-field col-lg-12">
            <input
              id="message"
              defaultValue={description}
              onChange={(e) => setDescription(e.target.value)}
              className="input-text js-input"
              type="text"
            />
            <label className="label" htmlFor="message">
              Description
            </label>
          </div>
          <div className="form-field col-lg-6 ">
            <input
              id="company"
              className="input-text js-input"
              defaultValue={realease}
              onChange={(e) => setRealease(e.target.value)}
              type="text"
            />
            <label className="label" htmlFor="company">
              Realease
            </label>
          </div>
          <div className="form-field col-lg-6 ">
            <input
              id="phone"
              className="input-text js-input"
              defaultValue={rating}
              onChange={(e) => setRating(e.target.value)}
              type="text"
            />
            <label className="label" htmlFor="phone">
              Rating
            </label>
          </div>
          <button onClick={save} className="btn btn-secondary" type="button">
            Save
          </button>
        </form>
      </section>
    </div>
  );
}

export default AddMovie;
