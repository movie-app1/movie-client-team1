import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import "../css/Login.css";

function RegisterPage() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userRegister, setUserRegister] = useState({
    username: "",
    password: "",
    role: "",
  });

  const handleOnChange = (e) => {
    setUserRegister((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const signUp = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userRegister),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.jwtToken);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/login");
        }
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div>
      <div className="">
        <div
          className="row no-gutters shadow-lg"
          style={{ backgroundColor: "#3a3a3a" }}
        >
          <div className="col-md-6 d-none d-md-block">
            <img
              src="https://cluster-images.visionplus.id/static/website/web_login.jpg"
              className="img-fluid"
              style={{ minHeight: "50%" }}
            />
          </div>
          <div
            className="col-md-6 text-white bg-dark p-5 d-flex align-self-center"
            style={{ backgroundColor: "#3a3a3a" }}
          >
            <div className="main">
              <input type="checkbox" id="chk" aria-hidden="true" />

              <div className="signup">
                <form>
                  <label htmlFor="chk" aria-hidden="true">
                    Register
                  </label>
                  <div className="form-group ">
                    <p className="text-light fs-6">Username</p>
                    <input
                      //   style={{ backgroundColor: "#3a3a3a" }}
                      type="text"
                      placeholder="Username"
                      className="form-control text-dark justify-content-center "
                      id="username"
                      aria-describedby="emailHelp"
                      onChange={handleOnChange}
                      defaultValue={userRegister.username}
                    />{" "}
                    <br />
                    <p className="text-light fs-6 ">Password</p>
                    <input
                      //   style={{ backgroundColor: "#3a3a3a" }}
                      type="password"
                      placeholder="Password"
                      className="form-control text-dark justify-content-center "
                      id="password"
                      aria-describedby="emailHelp"
                      onChange={handleOnChange}
                      defaultValue={userRegister.password}
                    />
                    <p className="text-light fs-6 mt-2">Role</p>
                    <input
                      //   style={{ backgroundColor: "#3a3a3a" }}
                      type="text"
                      placeholder="Role"
                      className="form-control text-dark justify-content-center "
                      id="role"
                      aria-describedby="emailHelp"
                      onChange={handleOnChange}
                      defaultValue={userRegister.role}
                    />
                  </div>
                  <button className="tombol" type="submit" onClick={signUp}>
                    Register
                  </button>
                </form>
                <Link
                  to="/login"
                  className="tombol btn btn-transparent bg-transparent"
                  type="submit"
                >
                  <u>Atau Login</u>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RegisterPage;
