import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/api";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { storage } from "../Firebase";

function UpdateGame() {
  const navigate = useNavigate();
  const { id } = useParams();
  // console.log(id);

  const [game, setGame] = useState("");
  const [image, setImage] = useState([]);

  const UpdateGame = async (downloadURL) => {
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        game: game,
        image: downloadURL,
      };

      Swal.fire({
        title: "Do you want to save changes?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `cancel,`,
      }).then((result) => {
        if (result.isConfirmed) {
          // console.log(data);
          axios.put(`game/${id}`, data);

          navigate("/game");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Menu changed successfully!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Changes are not saved!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  const save = () => {
    // untuk storage
    const storageRef = ref(storage, `games/${image.name}`);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadURL) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          UpdateGame(downloadURL);
          console.log(downloadURL);
        });
      });
  };

  const getDataById = async () => {
    const { data } = await axios.get(`/game/id/${id}`);

    setGame(data.game);
    setImage(data.image);
    // console.log(data);
  };

  useEffect(() => {
    getDataById();
  }, [id]);

  return (
    <div>
      <div>
        <section className="get-in-touch container">
          <h1 className="title fs-5 text-light">Update Game</h1>
          <form className="contact-form row">
            <div className="form-field col-lg-6">
              <input
                id="name"
                className="input-text js-input"
                defaultValue={game}
                onChange={(e) => setGame(e.target.value)}
                type="text"
              />
              <label className="label" htmlFor="name">
                Game
              </label>
            </div>
            <div className="form-field col-lg-6 ">
              <input
                type="file"
                className="form-control"
                id="inputGroupFile02"
                accept="image/png, image/jpeg, image/jpg"
                onChange={(e) => setImage(e.target.files[0])}
              />
              <label className="label" htmlFor="text">
                Image
              </label>
            </div>
            <div className="form-field col-lg-12">
              <button
                onClick={save}
                className="btn btn-secondary"
                type="button"
              >
                Save
              </button>
              {/* <input
                className="submit-btn"
                type="submit"
                onClick={UpdateGame}
              /> */}
            </div>
          </form>
        </section>
      </div>
    </div>
  );
}

export default UpdateGame;
