import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/api";

function CategoryChannel(props) {
  const { channel } = props;

  const removePost = async () => {
    await Swal.fire({
      title: "Do You Want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/tv/${channel.id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Succesfully Deleted Your Post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "canceled",
          text: "",
          howConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div className="container justify-content-center col-lg-2 col-6">
      <img
        src={channel.image}
        alt="Avatar"
        className="image rounded mx-auto d-block"
        style={{ width: "100%" }}
      />
      <div className="d-flex justify-content-between mt-1">
        {localStorage.getItem("role") === "admin" ? (
          <Link to={`/updateChannel/${channel.id}`}>
            <i
              style={{ fontSize: "3vw" }}
              className="fa-sharp fa-solid fa-pen-to-square text-warning text-center w-25"
            ></i>
          </Link>
        ) : (
          <div></div>
        )}
        {localStorage.getItem("role") === "admin" ? (
          <span>
            <i
              className="fa-solid fa-trash text-danger text-center"
              onClick={removePost}
              style={{ cursor: "pointer", fontSize: "3vw" }}
            ></i>
          </span>
        ) : (
          <div></div>
        )}
      </div>
      <div className="middle">
        <button className="text btn btn-primary">play</button>
      </div>
    </div>
  );
}

export default CategoryChannel;
