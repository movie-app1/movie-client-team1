import React from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import "../css/CarousellMovie.css";
import { instance as axios } from "../util/api";

function CarouselMovie(props) {
  const { movies } = props;

  const removePost = async () => {
    await Swal.fire({
      title: "Do You Want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/movie/${movies.id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Succesfully Deleted Your Post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "canceled",
          text: "",
          howConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };
  return (
    <div className="col-lg-2 col-4">
      <Link to={`/movie/${movies.id}`}>
        <div className="example">
          <div class="container movie d-flex align-self-center">
            <div className="container">
              <img
                src={movies.cover}
                alt="Avatar"
                class=" img-fluid responsive"
                width="100%"
              />
              <div class="overlay">
                <div class="text">{movies.title}</div>
              </div>
            </div>
          </div>
        </div>
        {localStorage.getItem("role") === "admin" ? (
          <div className="d-flex justify-content-between">
            <Link to={`/update/${movies.id}`}>
              <i className="fa-sharp fa-solid fa-pen-to-square text-warning text-center w-25"></i>
            </Link>{" "}
            {""}
            <span>
              <i
                className="fa-solid fa-trash text-danger text-center"
                onClick={removePost}
                style={{ cursor: "pointer" }}
              ></i>
            </span>
          </div>
        ) : (
          <div></div>
        )}
      </Link>
    </div>
  );
}

export default CarouselMovie;
