import React from "react";

function Mobile() {
  return (
    <div>
      <div className="container-fluid d-flex justify-content-center">
        <img
          src="https://www.visionplus.id/statics/placement/devices-webd.png"
          alt=""
          className="w-75"
        />
      </div>
    </div>
  );
}

export default Mobile;
