import { instance as axios } from "../util/api";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

function AddGenre() {
  const navigate = useNavigate();
  const [genre, setGenre] = useState(" ");

  const AddGenre = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const formData = {
        genre: genre,
      };

      await axios.post(`/genre/add`, formData);
      Swal.fire({
        icon: "success",
        title: "Good job!",
        text: "Category added successfully",
        showConfirmButton: false,
        timer: 1000,
      });
      navigate("/");
    } catch (err) {
      console.log(err);
    }
  };

  const save = (e) => {
    e.preventDefault();
    AddGenre();
  };

  // const save = async (e) => {
  //   e.preventDefault();
  //   await axios.post("genre/add", {
  //     genre: genre,
  //   });
  //   navigate.push("/");
  // };

  return (
    <div>
      <br />
      <br />
      <form className="text-center mb-5">
        <div className="field mt-3">
          <label className="label">Add Genre</label>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              placeholder="Genre"
              onChange={(e) => setGenre(e.target.value)}
            />
          </div>
        </div>
        <div className="field mt-3">
          <button onClick={save} className="btn btn-secondary" type="button">
            Save
          </button>
        </div>
      </form>
    </div>
  );
}

export default AddGenre;
