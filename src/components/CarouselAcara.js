import React from "react";

function CarousellAcara() {
  return (
    <div className="container-fluid p-2" style={{ border: "1px solid red" }}>
      <div className="d-flex text-light row">
        <div
          className="col-6 text-center d-flex align-self-center justify-content-center"
          style={{ fontSize: "30px" }}
        >
          Masterchef Indonesia season 10
          {/* <small className="text-secondary">Indonesian Idol</small> */}
        </div>
        <div className="col-6 d-flex ">
          <div
            id="carouselExampleFade"
            className="carousel slide carousel-fade"
            data-bs-ride="carousel"
          >
            <div className="carousel-inner">
              <div className="carousel-item active">
                <img
                  src="https://static.mncnow.id/images/channel/0a9f058e/43db_r32.jpg"
                  className="d-block w-100"
                  alt="..."
                />
              </div>
              <div className="carousel-item">
                <img
                  src="https://static.mncnow.id/images/series/0d9000bd/4720_r32.jpg"
                  className="d-block w-100"
                  alt="..."
                />
              </div>
              <div className="carousel-item">
                <img
                  src="https://static.mncnow.id/images/series/e86a8fdb/a51a_r32.jpg"
                  className="d-block w-100"
                  alt="..."
                />
              </div>
            </div>
            <button
              className="carousel-control-prev"
              type="button"
              data-bs-target="#carouselExampleFade"
              data-bs-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Previous</span>
            </button>
            <button
              className="carousel-control-next"
              type="button"
              data-bs-target="#carouselExampleFade"
              data-bs-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Next</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CarousellAcara;
