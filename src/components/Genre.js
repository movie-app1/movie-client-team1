import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { instance as axios } from "../util/api";

function Genre() {
  const [genres, setGenres] = useState([]);
  const { id } = useParams();

  const getGenre = async () => {
    try {
      const { data, status } = await axios.get(`/genre`);
      if (status === 200) {
        setGenres(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getGenre();
  }, []);

  //   const deleteGenre = async (id) => {
  //     await axios.delete(`genre/${id}`);
  //   };

  return (
    <div className="container-fluid justify-content-center d-flex">
      {genres.map((gen, i) => (
        <Link to={`/genre/id/${gen.id}`}>
          <div key={i}>
            <div
              className=" mx-1 my-2"
              style={{
                fontSize: "3vw",
              }}
            >
              <button className="btn btn-outline-light text-center">
                {gen.genre}
              </button>
            </div>
          </div>
        </Link>
      ))}
    </div>
  );
}

export default Genre;
