import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import "../css/Moviedetail.css";
import { instance as axios } from "../util/api";
import "video-react/dist/video-react.css";
import { Player } from "video-react";
import YouTubePlayer from "react-player/youtube";
import ReactPlayer from "react-player";

function MovieDetail() {
  const { id } = useParams();
  const [movies, setMovie] = useState([]);

  const fetchMovie = async () => {
    try {
      const { data, status } = await axios.get(`/movie/id/${id}`);
      if (status === 200) {
        setMovie(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchMovie();
  }, []);
  return (
    <div className="container tepian mt-3">
      <br />
      <div className="">
        {/* <Player> 
          <source url={movies.trailer} /> 
        </Player> */}
        {/* <Player playsInline poster="/assets/poster.png" url={movies.trailer} /> */}
        {/* <video type="video/mp4">src={movies.trailer}</video> */}
        <div className="m-3 p-3">
          <iframe
            className="img-fluid rounded mx-auto d-block"
            width="100%"
            style={{ height: "40vw" }}
            src={movies.trailer}
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowfullscreen
          ></iframe>
        </div>
        {/* <video src={movies.trailer} width="750" height="500" controls></video> */}
        {/* <video width="750" height="500" controls> 
          <source url={movies.trailer} type="video/mp4" /> 
        </video> */}
      </div>
      <div className="d-block justify-content-center mx-1">
        <h1 className="text-Breake text-light fs-4 mt-5 text-center">
          {movies.title}
        </h1>
        {/* <div className="d-flex justify-content-center ">
          <button className="btn btn-primary w-100 mb-1">Play</button>
        </div>
        <div className="d-flex justify-content-center ">
          <button className="btn btn-secondary w-100">Share</button>
        </div> */}

        <p className="text-Breake text-light fs-4">
          <p className="text-secondary">Deskripsi</p>

          {movies.description}
        </p>
        <p className="text-light text-start fs-4">
          <p className="text-secondary">
            Director: <p className="text-light">{movies.sutradara}</p>
          </p>
          <div className="d-flex justify-content-between">
            <p className="text-light">
              Rating: <br />
              {movies.rating}{" "}
            </p>
            <p className="text-light">
              Realese: <br />
              {movies.realease}{" "}
            </p>
          </div>
          {/* <p className="text-secondary justify-content-between"> 
            Rating: <p className="text-light"></p> 
          </p> */}
        </p>
        <p className="text-light text-start fs-4"></p>
      </div>
    </div>
  );
}

export default MovieDetail;
