import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import "../css/FormUpdate.css";
import { instance as axios } from "../util/api";
import {
  deleteObject,
  getDownloadURL,
  ref,
  uploadBytes,
} from "firebase/storage";
import { storage } from "../Firebase";

function UpdateMovie() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [cover, setCover] = useState("");
  const [trailer, setTrailer] = useState("");
  const [editCover, setEditCover] = useState([]);
  const [author, setAuthor] = useState("");
  const [rating, setRating] = useState("");
  const [realease, setRealease] = useState("");
  const [genre, setGenre] = useState([]);
  const [selectGenre, setSelectGenre] = useState("");

  const updateMovies = async (downloadURL) => {
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        title: title,
        description: description,
        cover: downloadURL,
        genres: { id: selectGenre },
        author: author,
        trailer: trailer,
        rating: rating,
        realease: realease,
      };

      Swal.fire({
        title: "Do you want to save changes?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Cancel`,
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data);
          axios.put(`/movie/${id}`, data);
          const storageRef = ref(storage, `images/${editCover.cover}`);

          // disinilah teman-teman menghapus image sebelumnya!
          deleteObject(storageRef);

          navigate("/");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Menu changed successfully!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Changes are not saved!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  const save = () => {
    // untuk storage
    const storageRef = ref(storage, `images/${cover.name}`);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, cover)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadURL) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          updateMovies(downloadURL);
          console.log(downloadURL);
        });
      });
  };

  const getDataById = async () => {
    const { data } = await axios.get(`movie/id/${id}`);
    // console.log(data);

    setTitle(data.title);
    setEditCover(data);
    setCover(data.cover);
    setTrailer(data.trailer);
    setAuthor(data.author);
    setDescription(data.description);
    setRating(data.rating);
    setRealease(data.realease);
  };

  useEffect(() => {
    getDataById();
  }, [id]);

  return (
    <div>
      <section className="get-in-touch container">
        <h1 className="title fs-5">Update Movie</h1>
        <form className="contact-form row">
          <div className="form-field col-lg-6">
            <input
              id="text"
              className="input-text js-input"
              defaultValue={title}
              onChange={(e) => setTitle(e.target.value)}
              type="text"
            />
            <label className="label" htmlFor="name">
              Title
            </label>
          </div>
          <div className="form-field col-lg-6 ">
            <input
              id="text"
              className="input-text js-input"
              defaultValue={author}
              onChange={(e) => setAuthor(e.target.value)}
              type="text"
            />
            <label className="label" htmlFor="text">
              Author
            </label>
          </div>
          <div className="form-field col-lg-4">
            <input
              id="company"
              className="input-text js-input"
              defaultValue={trailer}
              onChange={(e) => setTrailer(e.target.value)}
              type="text"
            />
            <label className="label" htmlFor="company">
              Trailer
            </label>
          </div>
          <div className="form-field col-lg-4">
            <input
              id="phone"
              className="input-text js-input"
              accept="image/png, image/jpeg, image/jpg"
              onChange={(e) => setCover(e.target.files[0])}
              type="file"
            />
            <label className="label" htmlFor="phone">
              Cover
            </label>
          </div>
          <div className="form-field col-lg-12">
            <input
              id="message"
              defaultValue={description}
              onChange={(e) => setDescription(e.target.value)}
              className="input-text js-input"
              type="text"
            />
            <label className="label" htmlFor="message">
              Description
            </label>
          </div>
          <div className="form-field col-lg-6 ">
            <input
              id="company"
              className="input-text js-input"
              defaultValue={realease}
              onChange={(e) => setRealease(e.target.value)}
              type="text"
            />
            <label className="label" htmlFor="company">
              Realease
            </label>
          </div>
          <div className="form-field col-lg-6 ">
            <input
              id="phone"
              className="input-text js-input"
              defaultValue={rating}
              onChange={(e) => setRating(e.target.value)}
              type="text"
            />
            <label className="label" htmlFor="phone">
              Rating
            </label>
          </div>
        </form>
        <div className="form-field col-lg-12">
          <input className="submit-btn" type="submit" onClick={save} />
        </div>
      </section>
    </div>
  );
}

export default UpdateMovie;
