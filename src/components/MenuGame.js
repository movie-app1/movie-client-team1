import React from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import "../css/MenuGame.css";
import { instance as axios } from "../util/api";

function MenuGame(props) {
  const { games } = props;
  const removePost = async () => {
    await Swal.fire({
      title: "Do You Want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/game/${games.id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Succesfully Deleted Your Post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "canceled",
          text: "",
          howConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };
  return (
    <div className="container mt-4">
      <div>
        <div className="row align-items-center mt-3">
          <div className="col text-center">
            <img src={games.image} className="" width="35%" height="35%" />
          </div>
          <div className="col text-start" style={{ fontSize: "20px" }}>
            {games.game}{" "}
            {localStorage.getItem("role") === "admin" ? (
              <button onClick={removePost} className="btn btn-danger">
                <i className="fa-solid fa-trash"></i>
              </button>
            ) : (
              <div></div>
            )}
            {localStorage.getItem("role") === "admin" ? (
              <Link
                to={`/updateGame/${games.id}`}
                className="btn btn-secondary"
              >
                <i className="fa-solid fa-pen-to-square"></i>
              </Link>
            ) : (
              <div></div>
            )}
          </div>
          <div className="col text-center">
            <button className="btn btn-primary w-50">Play</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MenuGame;
