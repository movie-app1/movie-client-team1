import React, { useEffect, useState } from "react";
import { Link, Navigate } from "react-router-dom";
import "../css/Search.css";
import "../css/Navbar.css";
import Add from "./Add";
import { instance as axios } from "../util/api";

function Navbar() {
  const [genres, setGenres] = useState([]);

  const getGenre = async () => {
    try {
      const { data, status } = await axios.get(`/genre`);
      if (status === 200) {
        setGenres(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const logout = () => {
    localStorage.removeItem("token");
    Navigate("/login");
  };

  useEffect(() => {
    getGenre();
  }, []);
  return (
    <div>
      <nav
        className="navbar navbar-expand-lg bg-dark fixed-top"
        data-bs-theme="dark"
      >
        <div
          className="container-fluid mx-5 my-1"
          style={{
            fontSize: "12px",
            fontFamily: "Verdana, Geneva, Tahoma, sans-serif",
          }}
        >
          <Link className="navbar-brand" to="/">
            <img
              src="https://cluster-images.visionplus.id/static/appcfg/logo_visionplus_web.png"
              alt=""
              height="25px"
              width="130px"
            />
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link
                  to="/tvshow"
                  className="nav-link active text-light"
                  aria-current="page"
                  href="#"
                >
                  Tv Show
                </Link>
              </li>
              <li>
                <li className="nav-item dropdown">
                  <a
                    className="dropdown nav-link dropdown-toggle text-light"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Category
                  </a>

                  <ul
                    className="dropdown-content dropdown-menu dropdown-menu-dark"
                    aria-labelledby="navbarDropdown"
                  >
                    {genres.map((gen, i) => (
                      <li key={i}>
                        <Link
                          className="dropdown-item"
                          to={`/genre/id/${gen.id}`}
                        >
                          <p value={gen.id}>{gen.genre}</p>
                        </Link>
                      </li>
                    ))}
                  </ul>
                </li>
              </li>

              <li className="nav-item">
                <Link to="/game" className="nav-link text-light" href="#">
                  Games+
                </Link>
              </li>
              {localStorage.getItem("role") === "admin" ? <Add /> : <div></div>}
              {/* {localStorage.getItem("role") === "user" ? (
                <li className="nav-item">
                  <Link to="/mylist" className="nav-link text-light">
                    | My List
                  </Link>
                </li>
              ) : (
                <div></div>
              )} */}
            </ul>
            <form className="d-flex" role="search">
              <input
                className="form-control"
                type="search"
                placeholder="Search"
                aria-label="Search"
                style={{ borderRadius: "30px" }}
              />
              <button className="btn" type="submit">
                <i className="fa-solid fa-magnifying-glass text-light fa-2xl"></i>
              </button>
            </form>
            <div className="auto" style={{ fontSize: "12px" }}>
              <Link
                className="btn btn-outline-primary mx-1 my-3"
                style={{ borderRadius: "40px" }}
                type="submit"
              >
                Premium package
              </Link>

              {localStorage.getItem("token") ? (
                <Link className="btn btn-outline-dark">
                  <i
                    className="fa-solid fa-right-from-bracket text-light"
                    onClick={logout}
                  ></i>
                </Link>
              ) : (
                <Link
                  className="btn btn-outline-primary my-3"
                  style={{ borderRadius: "40px" }}
                  type="submit"
                  to="/login"
                >
                  Sign In
                </Link>
              )}
              <br />
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
