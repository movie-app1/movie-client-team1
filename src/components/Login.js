import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import "../css/Login.css";

function LoginPage() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userLogin, setUserLogin] = useState({ username: "", password: "" });

  const handleOnChange = (e) => {
    setUserLogin((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const signIn = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userLogin),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.token);
        localStorage.setItem("id", data.userData.id);
        localStorage.setItem("role", data.userData.role);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/");
          Swal.fire({
            icon: "success",
            title: "Yeayy your Login Success",
            showConfirmButton: false,
            timer: 800,
          });
        }
      } else {
        Swal.fire({
          icon: "error",
          title: "Email or Password Wrong",
          showConfirmButton: false,
          timer: 800,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div className="">
      <div
        className="row no-gutters shadow-lg"
        style={{ backgroundColor: "#3a3a3a" }}
      >
        <div className="col-md-6 d-none d-md-block">
          <img
            src="https://cluster-images.visionplus.id/static/website/web_login.jpg"
            className="img-fluid"
            style={{ minHeight: "50%" }}
          />
        </div>
        <div
          className="col-md-6 text-white bg-dark p-5 d-flex align-self-center"
          style={{ backgroundColor: "#3a3a3a" }}
        >
          <div className="main">
            <input type="checkbox" id="chk" aria-hidden="true" />

            <div className="signup">
              <form>
                <label htmlFor="chk" aria-hidden="true">
                  Login
                </label>
                <div className="form-group ">
                  <p className="text-light fs-6">Username</p>
                  <input
                    // style={{ backgroundColor: "#3a3a3a" }}
                    type="text"
                    placeholder="Username"
                    className="form-control text-dark justify-content-center mx-auto"
                    id="username"
                    aria-describedby="emailHelp"
                    onChange={handleOnChange}
                    defaultValue={userLogin.username}
                  />
                  <p className="text-light fs-6 ">Password</p>
                  <input
                    // style={{ backgroundColor: "#3a3a3a" }}
                    type="password"
                    placeholder="Password"
                    className="form-control text-dark justify-content-center mx-auto"
                    id="password"
                    aria-describedby="emailHelp"
                    onChange={handleOnChange}
                    defaultValue={userLogin.password}
                  />
                </div>
                <button className="tombol" type="submit" onClick={signIn}>
                  Login
                </button>
                <Link
                  to="/register"
                  className="tombol btn btn-transparent bg-transparent"
                  type="submit"
                >
                  <u>Atau Register</u>
                </Link>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginPage;
