import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { instance as axios } from "../util/api";
import Swal from "sweetalert2";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { storage } from "../Firebase";

function AddGame() {
  const navigate = useNavigate();
  const [game, setGame] = useState("");
  const [image, setImage] = useState(null);

  const postGame = async (downloadUrl) => {
    try {
      const formData = {
        game: game,
        image: downloadUrl,
      };
      await axios.post(`game/add`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    navigate("/game");
  };
  const submit = (event) => {
    // untuk storage
    const storageRef = ref(storage, `games/${image.name}`);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadUrl) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          postGame(downloadUrl);
          console.log(downloadUrl);
        });
      });
  };
  const save = (e) => {
    e.preventDefault();
    submit();
    Swal.fire({
      icon: "success",
      title: "Succes Add Book",
      showConfirmButton: false,
      timer: 800,
    });
  };
  return (
    <div>
      <div>
        <section className="get-in-touch container">
          <h1 className="title fs-5">Add Game</h1>
          <form className="contact-form row">
            <div className="form-field col-lg-6">
              <input
                id="name"
                className="input-text js-input"
                defaultValue={game}
                onChange={(e) => setGame(e.target.value)}
                type="text"
              />
              <label className="label" htmlFor="name">
                Game
              </label>
            </div>
            <div className="form-field col-lg-6 ">
              <input
                type="file"
                className="form-control"
                id="inputGroupFile02"
                accept="image/png, image/jpeg, image/jpg"
                onChange={(e) => setImage(e.target.files[0])}
              />
              <label className="label" htmlFor="text">
                Image
              </label>
            </div>
            <div className="form-field col-lg-12">
              <button
                onClick={save}
                className="btn btn-secondary"
                type="button"
              >
                Save
              </button>
            </div>
          </form>
        </section>
      </div>
    </div>
  );
}

export default AddGame;
