// import axios from "axios";
import { instance as axios } from "../util/api";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { storage } from "../Firebase";

function AddChannel() {
  const navigate = useNavigate();
  const [tv, setTv] = useState("");
  const [image, setImage] = useState("");

  const postChannel = async (downloadURL) => {
    try {
      const formData = {
        tv: tv,
        image: downloadURL,
      };
      await axios.post(`tv/add`, formData, {});
    } catch (err) {
      console.log(err);
    }
    navigate("/tvshow");
  };
  const submit = (event) => {
    // untuk storage
    const storageRef = ref(storage, `Channels/${image.name}`);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadUrl) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          postChannel(downloadUrl);
          console.log(downloadUrl);
        });
      });
  };
  const save = (e) => {
    e.preventDefault();
    submit();
    Swal.fire({
      icon: "success",
      title: "Succes Add Book",
      showConfirmButton: false,
      timer: 800,
    });
  };
  return (
    <div>
      <div>
        <section className="get-in-touch container">
          <h1 className="title fs-5">Add Channel</h1>
          <form className="contact-form row">
            <div className="form-field col-lg-6">
              <input
                id="name"
                className="input-text js-input"
                defaultValue={tv}
                onChange={(e) => setTv(e.target.value)}
                type="text"
              />
              <label className="label" htmlFor="name">
                Channel
              </label>
            </div>
            <div className="form-field col-lg-6 ">
              <input
                type="file"
                className="form-control"
                id="inputGroupFile02"
                accept="image/png, image/jpeg, image/jpg"
                onChange={(e) => setImage(e.target.files[0])}
              />
              <label className="label" htmlFor="text">
                Image
              </label>
            </div>
            <div className="form-field col-lg-12">
              <button
                onClick={save}
                className="btn btn-secondary"
                type="button"
              >
                Save
              </button>
            </div>
          </form>
        </section>
      </div>
    </div>
  );
}

export default AddChannel;
