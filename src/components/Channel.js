import React from "react";

function Channel() {
  return (
    <div className="container">
      <div>
        <div className="row d-flex justify-content-center mt-2 mb-5">
          <div className="col-2 ">
            <a href="">
              <img
                className="bg-light w-75 rounded-circle mx-2"
                style={{
                  borderRadius: "50%",
                  backgroundColor: "white",
                }}
                src="https://static.visionplus.id/images/channel/c8aee159-49c.png"
                alt="chanel avatar"
              />
            </a>
          </div>
          <div className="col-2  ">
            <a href="">
              <img
                className="bg-light w-75 rounded-circle mx-2"
                style={{
                  borderRadius: "50%",
                  backgroundColor: "white",
                }}
                src="https://static.visionplus.id/images/channel/9d7415d1-425.png"
                alt="chanel avatar"
              />
            </a>
          </div>
          <div className="col-2  ">
            <a href="">
              <img
                className="bg-light w-75 rounded-circle mx-2"
                style={{
                  borderRadius: "50%",
                  backgroundColor: "white",
                }}
                src="https://static.visionplus.id/images/channel/c574e2d4-75e.png"
                alt="chanel avatar"
              />
            </a>
          </div>
          <div className="col-2  ">
            <a href="">
              <img
                className="bg-light w-75 rounded-circle mx-2"
                style={{
                  borderRadius: "50%",
                  backgroundColor: "white",
                }}
                src="https://static.mncnow.id/images//channel/8f231a4a-387.png"
                alt="chanel avatar"
              />
            </a>
          </div>
          <div className="col-2  ">
            <a href="">
              <img
                className="bg-light w-75 rounded-circle mx-2"
                style={{
                  borderRadius: "50%",
                  backgroundColor: "white",
                }}
                src="https://static.visionplus.id/images/channel/1b730421-25e.png"
                alt="chanel avatar"
              />
            </a>
          </div>
          <div className="col-2  ">
            <a href="">
              <img
                className="bg-light w-75 rounded-circle mx-2"
                style={{
                  borderRadius: "50%",
                  backgroundColor: "white",
                }}
                src="https://static.mncnow.id/images//channel/66544aea-676.png"
                alt="chanel avatar"
              />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Channel;
