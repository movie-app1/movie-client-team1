import React from "react";

function WatchEverywhere() {
  return (
    <div>
      <h1 className="text-center text-light m-2">Watch Everywhere</h1>
      <p className="text-center text-light">
        Stream more than 16.000 hours of Vision+ Originals, various <br />
        movies,series, and live national TV channels on your devices.
      </p>
      <div
        className="d-flex justify-content-center"
        style={{ fontSize: "12px" }}
      >
        <button
          className="btn btn-outline-light mx-1 my-3"
          style={{ borderRadius: "40px" }}
          type="submit"
        >
          Download App
        </button>
        <button
          className="btn btn-outline-light mx-1 my-3"
          style={{ borderRadius: "40px" }}
          type="submit"
        >
          Go To Vision+
        </button>
      </div>
    </div>
  );
}

export default WatchEverywhere;
