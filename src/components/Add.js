import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { instance as axios } from "../util/api";

function Add() {
  const [genres, setGenres] = useState([]);

  const getGenre = async () => {
    try {
      const { data, status } = await axios.get(`/genre`);
      if (status === 200) {
        setGenres(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getGenre();
  }, []);

  return (
    <div>
      <li className="nav-item dropdown">
        <a
          className="dropdown nav-link dropdown-toggle text-light"
          href="#"
          id="navbarDropdown"
          role="button"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          Add +
        </a>
        <ul
          className="dropdown-content dropdown-menu dropdown-menu-dark"
          aria-labelledby="navbarDropdown"
        >
          <li>
            <Link className="dropdown-item" to="/addMovie">
              Add Movie
            </Link>
          </li>
          <li>
            <Link className="dropdown-item" to="/addGenre">
              Add Genre
            </Link>
          </li>
          <li>
            <Link className="dropdown-item" to="/addGame">
              Add Game
            </Link>
          </li>
          <li>
            <Link className="dropdown-item" to="/addChannel">
              Add Channel
            </Link>
          </li>
        </ul>
      </li>
    </div>
  );
}

export default Add;
