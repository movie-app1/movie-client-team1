// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getStorage } from "firebase/storage";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD7k4xVZw6i9guPTat3AzF0Sw4spzz2PO4",
  authDomain: "movieapp-3e281.firebaseapp.com",
  projectId: "movieapp-3e281",
  storageBucket: "movieapp-3e281.appspot.com",
  messagingSenderId: "484062542532",
  appId: "1:484062542532:web:75b8dfc3273105888f6791",
  measurementId: "G-NV0V9ZJYQG",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Cloud Storage and get a reference to the service
export const storage = getStorage(app);

const analytics = getAnalytics(app);
