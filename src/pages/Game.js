import { instance as axios } from "../util/api";
import React, { useEffect, useState } from "react";
import CarouselGames from "../components/CarouselGames";
import MenuGame from "../components/MenuGame.js";
import { Link } from "react-router-dom";

function Game() {
  const [game, setGame] = useState([]);

  const fetchGame = async () => {
    try {
      const { data, status } = await axios.get(`game`, {});
      if (status === 200) {
        setGame(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchGame();
  }, []);

  return (
    <div className="text-light">
      <CarouselGames />
      {game.map((games, i) => (
        <MenuGame key={i} games={games} setGame={setGame} />
      ))}
    </div>
  );
}

export default Game;
