import React, { useEffect, useState } from "react";
import CarousellHome from "../components/HomeCarousel";
import CarouselMovie from "../components/CarouselMovie";
import Channel from "../components/Channel";
import Mobile from "../components/Mobile";
import WatchEverywhere from "../components/WatchEverywhere";
import { instance as axios } from "../util/api";
import Footer from "../components/Footer";
import Genre from "../components/Genre";
// import { instance as axios } from "../util/api";

function Home() {
  const [movie, setMovie] = useState([]);

  const fetchMovie = async () => {
    try {
      const { data, status } = await axios.get(`/movie`);
      if (status === 200) {
        setMovie(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchMovie();
  }, []);

  return (
    <div className="">
      <h1>hai</h1>
      <CarousellHome />
      <div className="mx-4">
        <Genre />
      </div>
      <div className="container-fluid">
        <div className="row w-100">
          {movie.map((movies, i) => (
            <CarouselMovie key={i} movies={movies} setMovie={setMovie} />
          ))}
        </div>
      </div>
      <br />
      <div
        className="my-3 mt-2 d-flex align-self-center"
        // style={{ border: "1px solid white" }}
      >
        <Channel />
      </div>

      <div>
        <WatchEverywhere />
      </div>
      <Mobile />
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default Home;
