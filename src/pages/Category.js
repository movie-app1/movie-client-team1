import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/api";
import "../css/CarousellMovie.css";
import Footer from "../components/Footer";

function Category() {
  const [selectGenres, setSelectGenres] = useState([]);
  const { id } = useParams();

  const fetchGendreId = async () => {
    try {
      const { data, status } = await axios.get(`genre/id/${id}`);
      if (status === 200) {
        setSelectGenres(data.movie);
        // console.log(data.id);
        // console.log(data.genre);
        // console.log(data.movie);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchGendreId();
  }, []);

  const deleteMenu = async () => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/genre/${id}`);
        Swal.fire("Deleted!", "Successfully deleted your post!", "success");
      } else if (result.isDenied) {
        Swal.fire("Canceled!", "", "error");
      }
    });
  };

  return (
    <div className="container">
      <div className="row my-5">
        <h1>haiiii</h1>
        {selectGenres.map((gnre, i) => (
          <div key={i} className="col-2">
            <Link to={`/movie/${gnre.id}`}>
              <div className="example">
                <div className="image-hover-text-container">
                  <div className="image-hover-image zoom">
                    <img src={gnre.cover} className="responsive" />
                  </div>
                  <div className="image-hover-text centered">
                    <div className="image-hover-text-bubble">
                      <span className="image-hover-text-title">
                        {gnre.author}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </Link>
            <hr className="text-white" />
          </div>
        ))}
        {/* <Footer /> */}
      </div>
    </div>
  );
}

export default Category;
