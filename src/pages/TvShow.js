import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { instance as axios } from "../util/api";
import CategoryChannel from "../components/CategoryChannel";
import "../css/CategoryChannel.css";
import Footer from "../components/Footer";
import Swal from "sweetalert2";

function TvShow() {
  const [channel, setChannel] = useState([]);

  const fetchMovie = async () => {
    try {
      const { data, status } = await axios.get(`/tv`);
      if (status === 200) {
        setChannel(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchMovie();
  }, []);
  return (
    <div className="container align-self-center">
      <h1>hai</h1>
      <h1>hai</h1>
      <div className="row">
        <h4 className="text-white">Tv Nasional</h4>
        {channel.map((channel, i) => (
          <CategoryChannel key={i} channel={channel} setChannel={setChannel} />
        ))}
      </div>

      <Footer />
    </div>
  );
}

export default TvShow;
